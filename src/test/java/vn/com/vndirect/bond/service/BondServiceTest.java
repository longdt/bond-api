package vn.com.vndirect.bond.service;

import org.junit.Test;
import vn.com.vndirect.bond.TestCase;
import vn.com.vndirect.bond.model.entity.Bond;
import vn.com.vndirect.bond.util.Futures;

import static org.junit.Assert.*;

public class BondServiceTest implements TestCase {

    @Test
    public void getBond() {
        BondService bondService = getTestComponent().testBondService();
        Bond bond = Futures.sync(bondService::getBond, "BID1_106");
        assertNotNull(bond);
        System.out.println(bond.getBondCode());
    }
}