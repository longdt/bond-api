package vn.com.vndirect.bond;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import vn.com.vndirect.bond.conf.AppModule;
import vn.com.vndirect.bond.conf.DaggerTestComponent;
import vn.com.vndirect.bond.conf.TestComponent;

public interface TestCase {
    class Holder {
        static {
            Json.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Json.mapper.registerModule(new JavaTimeModule());
            Json.mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        }
        private static final Vertx vertx = Vertx.vertx();
        private static final TestComponent DEFAULT_COMPONENT = DaggerTestComponent.builder().appModule(new AppModule(vertx)).build();

        private static TestComponent INSTANCE;

        private static synchronized TestComponent getInstance(AppModule appModule) {
            if (INSTANCE == null) {
                INSTANCE = DaggerTestComponent.builder().appModule(appModule).build();
            }
            return INSTANCE;
        }
    }

    default TestComponent getTestComponent() {
        return Holder.DEFAULT_COMPONENT;
    }

    default TestComponent getTestComponent(AppModule appModule) {
        return Holder.getInstance(appModule);
    }

}
