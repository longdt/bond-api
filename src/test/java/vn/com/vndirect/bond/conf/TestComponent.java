package vn.com.vndirect.bond.conf;

import dagger.Component;
import vn.com.vndirect.bond.repository.DealRepository;
import vn.com.vndirect.bond.repository.ProductRepository;
import vn.com.vndirect.bond.service.BondService;
import vn.com.vndirect.bond.service.ProductService;

import javax.inject.Singleton;

@Singleton
@Component(modules = AppModule.class)
public interface TestComponent {

    ProductService testProductService();

    ProductRepository testProductRepository();

    DealRepository testDealRepository();

    BondService testBondService();
}