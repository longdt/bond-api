package vn.com.vndirect.bond.http.handler;

import com.github.mauricio.async.db.postgresql.exceptions.GenericDatabaseException;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import vn.com.vndirect.bond.exception.*;
import vn.com.vndirect.bond.model.entity.Product;
import vn.com.vndirect.bond.model.request.PageRequest;
import vn.com.vndirect.bond.model.response.Page;
import vn.com.vndirect.bond.service.ProductService;
import vn.com.vndirect.bond.util.Responses;
import vn.com.vndirect.bond.validate.ProductValidator;

import javax.inject.Inject;
import java.util.Optional;

public class ProductHandler {
    private ProductService productService;
    private ProductValidator validator;

    @Inject
    public ProductHandler(ProductService productService, ProductValidator validator) {
        this.productService = productService;
        this.validator = validator;
    }

    public void createProduct(RoutingContext routingContext) {
        Product product;
        try {
            product = Json.decodeValue(routingContext.getBody(), Product.class);
            validator.validate(product);

        } catch (DecodeException e) {
            throw new InvalidFieldTypeException("Product object is invalid");
        }

        productService.createProduct(product, new ResponseHandler<Product>(routingContext) {
            @Override
            public void success(Product result) {
                Responses.ok(routingContext, result);
            }

            @Override
            public void fail(Throwable cause) throws Throwable {
                if (cause instanceof GenericDatabaseException) {
                    cause = new DuplicateIDException("Duplicate id error");
                }
                throw cause;
            }
        });
    }

    public void getProduct(RoutingContext routingContext) {
        String productId = routingContext.request().getParam("id");
        if (productId == null) {
            throw new InvalidProductIDException("product " + productId + " is invalid");
        }

        productService.getProduct(productId, new ResponseHandler<Optional<Product>>(routingContext) {
            @Override
            public void success(Optional<Product> result) {
                Responses.ok(routingContext, result.orElseThrow(
                        () -> new ProductNotFoundException("Product " + productId + " is not found")));
            }
        });
    }

    public void deleteProduct(RoutingContext routingContext) {
        String productId = routingContext.request().getParam("id");

        productService.deleteProduct(productId, new ResponseHandler<Void>(routingContext) {
            @Override
            public void success(Void result) throws Throwable {
                Responses.ok(routingContext);
            }

            @Override
            public void fail(Throwable cause) throws Throwable {
                if (cause instanceof EntityNotFoundException) {
                    cause = new ProductNotFoundException("Product " + productId + " is not found");
                }
                throw cause;
            }
        });
    }

    public void getProductPage(RoutingContext routingContext) {
        try {
            int indexPage = Integer.parseInt(routingContext.request().getParam("page"));
            int sizePage = Integer.parseInt(routingContext.request().getParam("size"));

            if (indexPage <= 0 || sizePage <= 0) {
                throw new InvalidPageRequestException("Page index must start from 1 and size must > 0");
            }

            PageRequest pageRequest = new PageRequest(indexPage, sizePage);

            productService.getProductPage(pageRequest, new ResponseHandler<Page<Product>>(routingContext) {
                @Override
                public void success(Page<Product> result) {
                    Responses.ok(routingContext, result);
                }
            });
        } catch (NumberFormatException e) {
            throw new InvalidPageRequestException("page=" + routingContext.request().getParam("page")
                    + " size=" + routingContext.request().getParam("size"), e);
        }
    }

    public void updateProduct(RoutingContext routingContext) {
        Product product;
        try {
            product = Json.decodeValue(routingContext.getBody(), Product.class);
            validator.validate(product);
            String productId = routingContext.request().getParam("id");
            product.setProductId(productId);
        } catch (DecodeException e) {
            throw new InvalidFieldTypeException("Product object is valid Error");
        }

        productService.updateProduct(product, new ResponseHandler<Product>(routingContext) {
            @Override
            public void success(Product result) throws Throwable {
                Responses.ok(routingContext, result);
            }
        });
    }

    public void getProductBy(RoutingContext routingContext) {
        String bondCode = routingContext.request().getParam("bondCode");
        String productType = routingContext.request().getParam("productType");

        productService.getProduct(productType, bondCode, new ResponseHandler<Optional<Product>>(routingContext) {
            @Override
            public void success(Optional<Product> result) throws Throwable {
                Responses.ok(routingContext, result.orElseThrow(() -> new ProductNotFoundException("Bug system: Bond code " + bondCode + " or Product type " + productType + " is not found" )));
            }
        });
    }
}
