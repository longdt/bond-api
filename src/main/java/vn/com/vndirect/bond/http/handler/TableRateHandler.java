package vn.com.vndirect.bond.http.handler;

import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import vn.com.vndirect.bond.exception.InvalidFieldTypeException;
import vn.com.vndirect.bond.exception.InvalidTableRateIdException;
import vn.com.vndirect.bond.exception.TableRateNotFoundException;
import vn.com.vndirect.bond.model.entity.TableRate;
import vn.com.vndirect.bond.model.request.TableRateRequest;
import vn.com.vndirect.bond.model.response.TableRateResponse;
import vn.com.vndirect.bond.service.TableRateService;
import vn.com.vndirect.bond.util.Responses;
import vn.com.vndirect.bond.validate.TableRateRequestValidator;
import vn.com.vndirect.bond.validate.TableRateValidator;

import javax.inject.Inject;
import java.util.Optional;

public class TableRateHandler {
    private TableRateService tableRateService;
    private TableRateRequestValidator validator;

    @Inject
    public TableRateHandler(TableRateService tableRateService, TableRateRequestValidator validator) {
        this.tableRateService = tableRateService;
        this.validator = validator;
    }

    public void createTableRate(RoutingContext routingContext) {
        TableRateRequest tableRateRequest = null;
        try {
            tableRateRequest = Json.decodeValue(routingContext.getBody(), TableRateRequest.class);
            validator.validate(tableRateRequest);
        } catch (DecodeException e) {
            throw new InvalidFieldTypeException("Invalid field type Error");
        }

        tableRateService.createTableRate(tableRateRequest, new ResponseHandler<TableRateResponse>(routingContext) {
            @Override
            public void success(TableRateResponse result) throws Throwable {
                Responses.ok(routingContext, result);
            }
        });

    }

    public void getTableRate(RoutingContext routingContext) {
        Long id;
        try {
            id = Long.parseLong(routingContext.request().getParam("id"));
        } catch (NumberFormatException e) {
            throw new InvalidTableRateIdException("Invalid table rate id Error");
        }

        tableRateService.getTableRate(id, new ResponseHandler<Optional<TableRate>>(routingContext) {
            @Override
            public void success(Optional<TableRate> result) throws Throwable {
                Responses.ok(routingContext, result.orElseThrow(
                        () -> new TableRateNotFoundException("Table rate id " + id + " is not found")
                ));
            }
        });
    }
}
