package vn.com.vndirect.bond.http.handler;

import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import vn.com.vndirect.bond.exception.DealNotFoundException;
import vn.com.vndirect.bond.exception.InvalidDealIDException;
import vn.com.vndirect.bond.exception.InvalidPageRequestException;
import vn.com.vndirect.bond.model.entity.Deal;
import vn.com.vndirect.bond.model.request.PageRequest;
import vn.com.vndirect.bond.model.response.Page;
import vn.com.vndirect.bond.service.DealService;
import vn.com.vndirect.bond.util.Responses;
import vn.com.vndirect.bond.validate.DealValidator;

import javax.inject.Inject;
import java.util.Optional;

public class DealHandler {
    private DealService dealService;
    private DealValidator validator;

    @Inject
    public DealHandler(DealService dealService, DealValidator validator) {
        this.dealService = dealService;
        this.validator = validator;
    }

    public void createDeal(RoutingContext routingContext) {
        Deal deal = Json.decodeValue(routingContext.getBody(), Deal.class);
        validator.validate(deal);
        dealService.createDeal(deal, new ResponseHandler<Deal>(routingContext) {
            @Override
            public void success(Deal result) {
                Responses.ok(routingContext, result);
            }
        });
    }

    public void getDeal(RoutingContext routingContext) {
        Long id;
        try {
            id = Long.valueOf(routingContext.request().getParam("id"));
        } catch (NumberFormatException e) {
            throw new InvalidDealIDException("Invalid deal id");
        }

        dealService.getDeal(id, new ResponseHandler<Optional<Deal>>(routingContext) {
            @Override
            public void success(Optional<Deal> result) {
                Responses.ok(routingContext, result.orElseThrow(
                        () -> new DealNotFoundException("Deal " + id + " is not found")
                ));
            }
        });
    }

    public void getDealPage(RoutingContext routingContext) {
        try {
            int index = Integer.parseInt(routingContext.request().getParam("page"));
            int size = Integer.parseInt(routingContext.request().getParam("size"));

            if (index <= 0 || size <= 0) {
                throw new InvalidPageRequestException("Page index must start from 1 and size must > 0");
            }

            dealService.getDealPage(new PageRequest(index, size), new ResponseHandler<Page<Deal>>(routingContext) {
                @Override
                public void success(Page<Deal> result) {
                    Responses.ok(routingContext, result);
                }
            });
        } catch (NumberFormatException e) {
            throw new InvalidPageRequestException("page=" + routingContext.request().getParam("page")
                    + " size=" + routingContext.request().getParam("size"), e);
        }
    }
}
