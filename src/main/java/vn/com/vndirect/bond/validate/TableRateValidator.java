package vn.com.vndirect.bond.validate;

import vn.com.vndirect.bond.exception.ErrorCode;
import vn.com.vndirect.bond.exception.ValidateException;
import vn.com.vndirect.bond.model.entity.TableRate;
import static vn.com.vndirect.bond.validate.Validator.*;

public class TableRateValidator implements Validator<TableRate> {
    @Override
    public TableRate validate(TableRate tableRate) throws ValidateException {
        requireNonEmpty(tableRate.getName(),() -> new ValidateException(ErrorCode.REQUIRE_TR_TERM_NAME_ERROR));
        return tableRate;
    }
}
