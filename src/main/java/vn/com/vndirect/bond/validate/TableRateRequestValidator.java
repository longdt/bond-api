package vn.com.vndirect.bond.validate;

import vn.com.vndirect.bond.exception.ErrorCode;
import vn.com.vndirect.bond.exception.ValidateException;
import vn.com.vndirect.bond.model.entity.TableRate;
import vn.com.vndirect.bond.model.request.TableRateRequest;
import static vn.com.vndirect.bond.validate.Validator.*;


public class TableRateRequestValidator implements Validator<TableRateRequest> {

    private TableRateValidator tableRateValidator = new TableRateValidator();
    private TermRateValidator termRateValidator = new TermRateValidator();

    @Override
    public TableRateRequest validate(TableRateRequest tableRateRequest) throws ValidateException {
        requireNonEmpty(tableRateRequest.getTerms(), () -> new ValidateException(ErrorCode.REQUIRE_TR_TERMS_ERROR));
        tableRateValidator.validate(tableRateRequest.getTableRate());
        tableRateRequest.getTerms().forEach(termRate -> termRateValidator.validate(termRate));
        return tableRateRequest;
    }
}
