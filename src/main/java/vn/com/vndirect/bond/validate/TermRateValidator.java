package vn.com.vndirect.bond.validate;

import vn.com.vndirect.bond.exception.ErrorCode;
import vn.com.vndirect.bond.exception.ValidateException;
import vn.com.vndirect.bond.model.entity.TermRate;
import static vn.com.vndirect.bond.validate.Validator.*;

public class TermRateValidator implements Validator<TermRate> {
    @Override
    public TermRate validate(TermRate termRate) throws ValidateException {
        requireNonNegative(termRate.getRate(), () -> new ValidateException(ErrorCode.REQUIRE_TR_RATE_ERROR));
        requirePositive(termRate.getTerm(), () -> new ValidateException(ErrorCode.REQUIRE_TR_TERM_ERROR));
        requireNonNull(termRate.getTermUnit(), () -> new ValidateException(ErrorCode.REQUIRE_TR_TERM_UNIT_ERROR));
        return termRate;
    }
}
