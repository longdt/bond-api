package vn.com.vndirect.bond.validate;

import vn.com.vndirect.bond.exception.ErrorCode;
import vn.com.vndirect.bond.exception.ValidateException;
import vn.com.vndirect.bond.model.entity.Product;

import static vn.com.vndirect.bond.validate.Validator.*;

public class ProductValidator implements Validator<Product> {
    @Override
    public Product validate(Product product) throws ValidateException {
        requireNull(product.getProductId(), () -> new ValidateException(ErrorCode.PRODUCT_ID_ALLOW));
        requireNonEmpty(product.getBondCode(), () -> new ValidateException(ErrorCode.REQUIRE_PROD_BONE_CODE_ERROR));
        requireNonNull(product.getProductType(), () -> new ValidateException(ErrorCode.REQUIRE_PROD_TYPE_ERROR));
        requireNonNegative(product.getLimit(), () -> new ValidateException(ErrorCode.REQUIRE_PROD_LIMIT_ERROR));
        requireNonNegative(product.getPromotion(), () -> new ValidateException(ErrorCode.REQUIRE_PROD_PROMOTION_ERROR));
        return product;
    }
}
