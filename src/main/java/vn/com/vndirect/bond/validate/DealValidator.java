package vn.com.vndirect.bond.validate;

import vn.com.vndirect.bond.exception.ErrorCode;
import vn.com.vndirect.bond.exception.ValidateException;
import vn.com.vndirect.bond.model.entity.Deal;

import static vn.com.vndirect.bond.validate.Validator.requireNonEmpty;

public class DealValidator implements Validator<Deal> {

    @Override
    public Deal validate(Deal deal) throws ValidateException {
        requireNonEmpty(deal.getBondCode(), () -> new ValidateException(ErrorCode.INVALID_BOND_CODE));
        return deal;
    }
}
