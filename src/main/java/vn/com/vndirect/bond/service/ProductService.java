package vn.com.vndirect.bond.service;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import vn.com.vndirect.bond.model.entity.Product;
import vn.com.vndirect.bond.model.request.PageRequest;
import vn.com.vndirect.bond.model.response.Page;
import vn.com.vndirect.bond.util.Futures;

import java.util.Optional;

public interface ProductService {

    void createProduct(Product product, Handler<AsyncResult<Product>> resultHandler);

    default Future<Product> createProduct(Product product) {
        return Futures.toFuture(this::createProduct, product);
    }

    void getProduct(String id, Handler<AsyncResult<Optional<Product>>> productResponseHandler);

    default Future<Optional<Product>> getProduct(String id) {
        return Futures.toFuture(this::getProduct, id);
    }

    void getProductPage(PageRequest pageRequest, Handler<AsyncResult<Page<Product>>> resultHandler);

    default Future<Page<Product>> getProductPage(PageRequest pageRequest) {
        return Futures.toFuture(this::getProductPage, pageRequest);
    }

    void deleteProduct(String id, Handler<AsyncResult<Void>> resultHandler);

    default Future<Void> deleteProduct(String id) {
        return Futures.toFuture(this::deleteProduct, id);
    }

    void updateProduct(Product product, Handler<AsyncResult<Product>> resultHandler);

    default Future<Product> updateProduct(Product productRequest) {
        return Futures.toFuture(this::updateProduct, productRequest);
    }

    void getProduct(String productType, String bondCode, Handler<AsyncResult<Optional<Product>>> resultHandler);

    default Future<Optional<Product>> getProduct(String productType, String bondCode) {
        return Futures.toFuture(this::getProduct, productType, bondCode);
    }
}
