package vn.com.vndirect.bond.service.impl;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import vn.com.vndirect.bond.model.entity.Bond;
import vn.com.vndirect.bond.model.entity.Deal;
import vn.com.vndirect.bond.model.request.PageRequest;
import vn.com.vndirect.bond.model.response.Page;
import vn.com.vndirect.bond.repository.DealRepository;
import vn.com.vndirect.bond.service.BondService;
import vn.com.vndirect.bond.service.DealService;

import javax.inject.Inject;
import java.util.Optional;

public class DealServiceImpl implements DealService {
    private DealRepository dealRepository;
    private BondService bondService;

    @Inject
    public DealServiceImpl(DealRepository dealRepository, BondService bondService) {
        this.dealRepository = dealRepository;
        this.bondService = bondService;
    }

    @Override
    public void createDeal(Deal deal, Handler<AsyncResult<Deal>> resultHandler) {
        bondService.getBond(deal.getBondCode()).compose(bond -> dealRepository.save(deal)).setHandler(resultHandler);
    }

    @Override
    public void getDeal(Long id, Handler<AsyncResult<Optional<Deal>>> resultHandler) {
        dealRepository.find(id, resultHandler);
    }

    @Override
    public void getDealPage(PageRequest pageRequest, Handler<AsyncResult<Page<Deal>>> resultHandler) {
        dealRepository.getPage(pageRequest, resultHandler);
    }
}
