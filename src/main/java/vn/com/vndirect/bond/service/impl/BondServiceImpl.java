package vn.com.vndirect.bond.service.impl;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import vn.com.vndirect.bond.conf.AppConf;
import vn.com.vndirect.bond.exception.BondNotFoundException;
import vn.com.vndirect.bond.model.entity.Bond;
import vn.com.vndirect.bond.service.BondService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class BondServiceImpl implements BondService {
    private WebClient client;
    private AsyncLoadingCache<String, Bond> bondCache;
    private final String getBondEndpoint;


    @Inject
    public BondServiceImpl(Vertx vertx, AppConf appConf) {
        AppConf.BoApiConf boApiConf = appConf.getBoApiConf();
        getBondEndpoint = boApiConf.getGetBondEndpoint();
        WebClientOptions options = new WebClientOptions().setDefaultHost(boApiConf.getHost())
                .setDefaultPort(boApiConf.getPort());
        client = WebClient.create(vertx, options);
        bondCache = Caffeine.newBuilder()
                .maximumSize(10_000)
                .expireAfterWrite(10, TimeUnit.HOURS)
                .buildAsync((key, executor) -> {
                    CompletableFuture<Bond> future = new CompletableFuture<>();
                    retrieveBond(key, ar -> {
                        if (ar.succeeded()) {
                            future.complete(ar.result());
                        } else {
                            future.completeExceptionally(ar.cause());
                        }
                    });
                    return future;
                });
    }

    @Override
    public void retrieveBond(String bondCode, Handler<AsyncResult<Bond>> resultHandler) {
        client.get(getBondEndpoint + "/" + bondCode)
                .send(ar -> {
                    if (ar.succeeded()) {
                        HttpResponse<Buffer> response = ar.result();
                        if (response.statusCode() == 200) {
                            Bond bond = response.bodyAsJson(Bond.class);
                            resultHandler.handle(Future.succeededFuture(bond));
                        } else if (response.statusCode() == 400) {
                            resultHandler.handle(Future.failedFuture(new BondNotFoundException(response.bodyAsString())));
                        }
                    } else {
                        resultHandler.handle(Future.failedFuture(ar.cause()));
                    }
                });
    }

    @Override
    public void getBond(String bondCode, Handler<AsyncResult<Bond>> resultHandler) {
        bondCache.get(bondCode).whenComplete((bond, e) -> {
            Future<Bond> result = e == null ? Future.succeededFuture(bond) : Future.failedFuture(e);
            resultHandler.handle(result);
        });
    }
}
