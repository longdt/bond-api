package vn.com.vndirect.bond.service.impl;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import vn.com.vndirect.bond.model.entity.Product;
import vn.com.vndirect.bond.model.request.PageRequest;
import vn.com.vndirect.bond.model.response.Page;
import vn.com.vndirect.bond.repository.ProductRepository;
import vn.com.vndirect.bond.repository.query.Query;
import vn.com.vndirect.bond.service.BondService;
import vn.com.vndirect.bond.service.ProductService;

import javax.inject.Inject;
import java.time.OffsetDateTime;
import java.util.Optional;

import static vn.com.vndirect.bond.repository.query.QueryFactory.*;

public class ProductServiceImpl implements ProductService {
    private ProductRepository productRepository;
    private BondService bondService;

    @Inject
    public ProductServiceImpl(ProductRepository productRepository, BondService bondService) {
        this.productRepository = productRepository;
        this.bondService = bondService;
    }

    @Override
    public void createProduct(Product product, Handler<AsyncResult<Product>> resultHandler) {
        product.setProductId(product.getBondCode() + "_" + product.getProductType());
        OffsetDateTime now = OffsetDateTime.now();
        product.setCreatedDate(now);
        product.setUpdatedDate(now);
        bondService.getBond(product.getBondCode()).compose(bond -> productRepository.insert(product)).setHandler(resultHandler);
    }

    @Override
    public void getProduct(String id, Handler<AsyncResult<Optional<Product>>> resultHandler) {
        productRepository.find(id, resultHandler);
    }

    @Override
    public void getProductPage(PageRequest pageRequest, Handler<AsyncResult<Page<Product>>> resultHandler) {
        productRepository.getPage(pageRequest, resultHandler);
    }

    @Override
    public void deleteProduct(String id, Handler<AsyncResult<Void>> resultHandler) {
        productRepository.remove(id, resultHandler);
    }

    @Override
    public void updateProduct(Product product, Handler<AsyncResult<Product>> resultHandler) {
        OffsetDateTime now = OffsetDateTime.now();
        product.setUpdatedDate(now);
        productRepository.update(product, resultHandler);
    }

    @Override
    public void getProduct(String productType, String bondCode, Handler<AsyncResult<Optional<Product>>> resultHandler) {
        Query<Product> query = or(
                        and(equal("productType", productType), equal("bondCode", bondCode)),
                        and(equal("productType", productType), isNull("bondCode"))
                );
        productRepository.querySingle(query, resultHandler);
    }

}
