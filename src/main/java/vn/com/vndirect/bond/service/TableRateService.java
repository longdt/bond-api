package vn.com.vndirect.bond.service;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import vn.com.vndirect.bond.model.entity.TableRate;
import vn.com.vndirect.bond.model.request.TableRateRequest;
import vn.com.vndirect.bond.model.response.TableRateResponse;
import vn.com.vndirect.bond.util.Futures;

import java.util.Optional;

public interface TableRateService {

    void createTableRate(TableRateRequest tableRateRequest, Handler<AsyncResult<TableRateResponse>> resultHandler);

    default Future<TableRateResponse> createTableRate(TableRateRequest tableRateRequest) {
        return Futures.toFuture(this::createTableRate, tableRateRequest);
    }

    void getTableRate(Long id, Handler<AsyncResult<Optional<TableRate>>> resultHandler);

    default Future<Optional<TableRate>> getTableRate(Long id){
        return Futures.toFuture(this::getTableRate, id);
    }
}
