package vn.com.vndirect.bond.service;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import vn.com.vndirect.bond.model.entity.Deal;
import vn.com.vndirect.bond.model.request.PageRequest;
import vn.com.vndirect.bond.model.response.Page;
import vn.com.vndirect.bond.util.Futures;

import java.util.Optional;

public interface DealService {
    void createDeal(Deal deal, Handler<AsyncResult<Deal>> resultHandler);

    default Future<Deal> createDeal(Deal deal) {
        return Futures.toFuture(this::createDeal, deal);
    }

    void getDeal(Long id, Handler<AsyncResult<Optional<Deal>>> resultHandler);

    default Future<Optional<Deal>> getDeal(Long id) {return  Futures.toFuture(this::getDeal, id);}

    void getDealPage(PageRequest pageRequest, Handler<AsyncResult<Page<Deal>>> resultHandler);

    default Future<Page<Deal>> getDealPage(PageRequest pageRequest) {
        return Futures.toFuture(this::getDealPage, pageRequest);
    }
}
