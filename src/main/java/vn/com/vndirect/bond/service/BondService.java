package vn.com.vndirect.bond.service;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import vn.com.vndirect.bond.model.entity.Bond;
import vn.com.vndirect.bond.util.Futures;

public interface BondService {
    void retrieveBond(String bondCode, Handler<AsyncResult<Bond>> resultHandler);

    default Future<Bond> retrieveBond(String bondCode) {
        return Futures.toFuture(this::retrieveBond, bondCode);
    }

    void getBond(String bondCode, Handler<AsyncResult<Bond>> resultHandler);

    default Future<Bond> getBond(String bondCode) {
        return Futures.toFuture(this::getBond, bondCode);
    }
}
