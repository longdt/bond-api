package vn.com.vndirect.bond.service.impl;

import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import vn.com.vndirect.bond.model.entity.TableRate;
import vn.com.vndirect.bond.model.entity.TermRate;
import vn.com.vndirect.bond.model.request.TableRateRequest;
import vn.com.vndirect.bond.model.response.TableRateResponse;
import vn.com.vndirect.bond.repository.TableRateRepository;
import vn.com.vndirect.bond.repository.TermRateRepository;
import vn.com.vndirect.bond.service.TableRateService;

import javax.inject.Inject;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TableRateServiceImpl implements TableRateService {

    private TableRateRepository tableRateRepository;
    private TermRateRepository termRateRepository;

    @Inject
    public TableRateServiceImpl(TableRateRepository tableRateRepository, TermRateRepository termRateRepository) {
        this.tableRateRepository = tableRateRepository;
        this.termRateRepository = termRateRepository;
    }

    @Override
    public void createTableRate(TableRateRequest tableRateRequest, Handler<AsyncResult<TableRateResponse>> resultHandler) {
        OffsetDateTime now = OffsetDateTime.now();
        tableRateRequest.getTableRate().setCreatedDate(now);
        tableRateRequest.getTableRate().setUpdatedDate(now);
        Future<TableRate> tableRateFuture = tableRateRepository.insert(tableRateRequest.getTableRate());
        tableRateFuture.compose(tableRate -> {
            List<TermRate> tr = tableRateRequest.getTerms();
            List<Future> termFuture = tr.stream().map(termRate -> {
                termRate.setTableRateId(tableRate.getTableRateId());
                termRate.setCreatedDate(now);
                termRate.setUpdatedDate(now);
                return termRateRepository.save(termRate);
            }).collect(Collectors.toList());
            return CompositeFuture.all(termFuture).map(t->{
                TableRateResponse response = new TableRateResponse();
                response.setTableRate(tableRate);
                response.setTerms(t.list());
                return response;
            });
        }).setHandler(resultHandler);
    }

    @Override
    public void getTableRate(Long id, Handler<AsyncResult<Optional<TableRate>>> resultHandler) {
       tableRateRepository.find(id, resultHandler);
    }

}
