package vn.com.vndirect.bond.conf;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.json.JsonObject;

public class AppConf {
    private String appName;
    @JsonProperty("boapi")
    private BoApiConf boApiConf;
    private JsonObject jsonObject;

    public AppConf() {
    }

    public AppConf(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public BoApiConf getBoApiConf() {
        return boApiConf;
    }

    public void setBoApiConf(BoApiConf boApiConf) {
        this.boApiConf = boApiConf;
    }

    public JsonObject getDatasource() {
        return jsonObject.getJsonObject("datasource");
    }

    AppConf setJsonObject(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
        return this;
    }

    public JsonObject getJsonObject() {
        return jsonObject;
    }

    public static class BoApiConf {
        private String host;
        private int port;
        @JsonProperty("get.bond.endpoint")
        private String getBondEndpoint;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public int getPort() {
            return port;
        }

        public void setPort(int port) {
            this.port = port;
        }

        public String getGetBondEndpoint() {
            return getBondEndpoint;
        }

        public void setGetBondEndpoint(String getBondEndpoint) {
            this.getBondEndpoint = getBondEndpoint;
        }
    }

}
