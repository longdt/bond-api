package vn.com.vndirect.bond.conf;

import dagger.Module;
import dagger.Provides;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vn.com.vndirect.bond.model.entity.TableRate;
import vn.com.vndirect.bond.repository.DealRepository;
import vn.com.vndirect.bond.repository.ProductRepository;
import vn.com.vndirect.bond.repository.TableRateRepository;
import vn.com.vndirect.bond.repository.TermRateRepository;
import vn.com.vndirect.bond.repository.impl.DealRepositoryImpl;
import vn.com.vndirect.bond.repository.impl.ProductRepositoryImpl;
import vn.com.vndirect.bond.repository.impl.TableRateRepositoryImpl;
import vn.com.vndirect.bond.repository.impl.TermRateRepositoryImpl;
import vn.com.vndirect.bond.service.BondService;
import vn.com.vndirect.bond.service.DealService;
import vn.com.vndirect.bond.service.ProductService;
import vn.com.vndirect.bond.service.TableRateService;
import vn.com.vndirect.bond.service.impl.BondServiceImpl;
import vn.com.vndirect.bond.service.impl.DealServiceImpl;
import vn.com.vndirect.bond.service.impl.ProductServiceImpl;
import vn.com.vndirect.bond.service.impl.TableRateServiceImpl;
import vn.com.vndirect.bond.util.Futures;
import vn.com.vndirect.bond.validate.*;

import javax.inject.Singleton;

@Module
public class AppModule {
    private static final Logger logger = LogManager.getLogger(AppModule.class);
    private Vertx vertx;

    public AppModule(Vertx vertx) {
        this.vertx = vertx;
    }

    @Singleton
    @Provides
    AppConf provideAppConf(Vertx vertx) {
        ConfigStoreOptions store = new ConfigStoreOptions()
                .setType("file")
                .setFormat("yaml")
                .setConfig(new JsonObject().put("path", "conf/app.yml"));

        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(store));
        Future<JsonObject> future = ConfigRetriever.getConfigAsFuture(retriever);
        Future<AppConf> appConf = future.map(jsonObject -> jsonObject.mapTo(AppConf.class).setJsonObject(jsonObject));
        return Futures.join(appConf);
    }

    @Singleton
    @Provides
    SQLClient provideSQLClient(AppConf appConf, Vertx vertx) {
        return PostgreSQLClient.createShared(vertx, appConf.getDatasource());
    }

    @Singleton
    @Provides
    TableRateRepository provideTableRateRepository(SQLClient sqlClient) {
        return new TableRateRepositoryImpl(sqlClient);
    }

    @Singleton
    @Provides
    ProductRepository provideProductRepository(SQLClient sqlClient) {
        return new ProductRepositoryImpl(sqlClient);
    }

    @Singleton
    @Provides
    ProductService provideProductService(ProductRepository productRepository, BondService bondService) {
        return new ProductServiceImpl(productRepository, bondService);
    }

    @Singleton
    @Provides
    DealRepository provideDealRepository(SQLClient sqlClient) {
        return new DealRepositoryImpl(sqlClient);
    }

    @Singleton
    @Provides
    DealService provideDealService(DealRepository dealRepository, BondService bondService) {
        return new DealServiceImpl(dealRepository, bondService);
    }

    @Singleton
    @Provides
    TableRateService tableRateService(TableRateRepository tableRateRepository, TermRateRepository termRateRepository) {
        return new TableRateServiceImpl(tableRateRepository, termRateRepository);
    }

    @Singleton
    @Provides
    BondService provideBondService(Vertx vertx, AppConf appConf) {
        return new BondServiceImpl(vertx, appConf);
    }

    @Singleton
    @Provides
    DealValidator provideDealValidator() {
        return new DealValidator();
    }

    @Singleton
    @Provides
    ProductValidator provideProductValidator() {
        return new ProductValidator();
    }

    @Singleton
    @Provides
    TableRateRequestValidator provideTableRateRequestValidator() {
        return new TableRateRequestValidator();
    }

    @Singleton
    @Provides
    TableRateValidator provideTableRateValidator() {
        return new TableRateValidator();
    }

    @Singleton
    @Provides
    TermRateRepository provideTermRateRepository(SQLClient sqlClient) {
        return new TermRateRepositoryImpl(sqlClient);
    }

    @Singleton
    @Provides
    TermRateValidator provideTermRateValidator() {
        return new TermRateValidator();
    }

    @Singleton
    @Provides
    Vertx provideVertx() {
       return vertx;
    }
}
