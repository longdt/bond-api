package vn.com.vndirect.bond.conf;

import dagger.Component;
import vn.com.vndirect.bond.http.handler.DealHandler;
import vn.com.vndirect.bond.http.handler.ProductHandler;
import vn.com.vndirect.bond.http.handler.TableRateHandler;
import vn.com.vndirect.bond.model.entity.TableRate;

import javax.inject.Singleton;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    ProductHandler createProductHandler();

    DealHandler createDealHandler();

    TableRateHandler createTableRateHandler();
}
