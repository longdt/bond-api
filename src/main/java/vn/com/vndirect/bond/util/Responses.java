package vn.com.vndirect.bond.util;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;

public class Responses {
    public static void ok(RoutingContext routingContext, Object response) {
        json(routingContext, 200, response);
    }
    public static void ok(RoutingContext routingContext) {
        json(routingContext).end();
    }
    public static void okMessage(RoutingContext routingContext, String message) {
        json(routingContext, 200, message);
    }

    public static void badRequest(RoutingContext routingContext, Object response) {
        json(routingContext, 400, response);
    }

    public static void notFound(RoutingContext routingContext, Object response) {
        json(routingContext, 404, response);
    }

    public static void internalServerError(RoutingContext routingContext, Object response) {
        json(routingContext, 500, response);
    }

    public static void json(RoutingContext routingContext, int httpStatus, Object response) {
        json(routingContext).setStatusCode(httpStatus).end(Json.encodeToBuffer(response));
    }

    public static HttpServerResponse json(RoutingContext routingContext) {
        return routingContext.response().putHeader("Content-type", "application/json");
    }
}
