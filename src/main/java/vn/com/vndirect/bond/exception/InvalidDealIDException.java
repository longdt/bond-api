package vn.com.vndirect.bond.exception;

public class InvalidDealIDException extends BusinessException {
    public InvalidDealIDException(String message) { super(ErrorCode.INVALID_DEAL_ID, message); }
}
