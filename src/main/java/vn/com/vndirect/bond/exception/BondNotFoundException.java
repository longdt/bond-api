package vn.com.vndirect.bond.exception;

public class BondNotFoundException extends BusinessException {
    public BondNotFoundException(String message) {
        super(ErrorCode.BOND_NOT_FOUND, message);
    }

    public BondNotFoundException(String message, Throwable cause) {
        super(ErrorCode.BOND_NOT_FOUND, message, cause);
    }

    public BondNotFoundException(Throwable cause) {
        super(ErrorCode.BOND_NOT_FOUND, cause);
    }
}
