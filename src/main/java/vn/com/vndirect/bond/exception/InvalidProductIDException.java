package vn.com.vndirect.bond.exception;

public class InvalidProductIDException extends BusinessException {
    public InvalidProductIDException(String message) {
        super(ErrorCode.INVALID_PRODUCT_ID, message);
    }

    public InvalidProductIDException(String message, Throwable cause) {
        super(ErrorCode.INVALID_PRODUCT_ID, message, cause);
    }

    public InvalidProductIDException(Throwable cause) {
        super(ErrorCode.INVALID_PRODUCT_ID, cause);
    }
}
