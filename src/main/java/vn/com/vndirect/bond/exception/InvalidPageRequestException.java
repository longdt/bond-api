package vn.com.vndirect.bond.exception;

public class InvalidPageRequestException extends BusinessException {
    public InvalidPageRequestException(String message) {
        super(ErrorCode.INVALID_PAGE_REQUEST, message);
    }

    public InvalidPageRequestException(String message, Throwable cause) {
        super(ErrorCode.INVALID_PAGE_REQUEST, message, cause);
    }

    public InvalidPageRequestException(Throwable cause) {
        super(ErrorCode.INVALID_PAGE_REQUEST, cause);
    }
}
