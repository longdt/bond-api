package vn.com.vndirect.bond.exception;

public class DealNotFoundException extends BusinessException {
    public DealNotFoundException(String message) { super(ErrorCode.DEAL_NOT_FOUND, message); }
}
