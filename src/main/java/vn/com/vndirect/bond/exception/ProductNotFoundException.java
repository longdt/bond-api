package vn.com.vndirect.bond.exception;

public class ProductNotFoundException extends BusinessException {

    public ProductNotFoundException(String message) {
        super(ErrorCode.PRODUCT_NOT_FOUND, message);
    }
}
