package vn.com.vndirect.bond.exception;

public class InvalidTableRateIdException extends BusinessException {
    public InvalidTableRateIdException(String message) {
        super(ErrorCode.INVALID_TABLE_RATE_ID, message);
    }
}
