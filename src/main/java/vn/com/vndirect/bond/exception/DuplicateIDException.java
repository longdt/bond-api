package vn.com.vndirect.bond.exception;

public class DuplicateIDException extends BusinessException {
    public DuplicateIDException(String message) {
        super(ErrorCode.DUPLICATE_ID_ERROR, message);
    }
}
