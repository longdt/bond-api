package vn.com.vndirect.bond.exception;

public class TableRateNotFoundException extends BusinessException {

    public TableRateNotFoundException(String message) {
        super(ErrorCode.TABLERATE_NOT_FOUND, message);
    }
}
