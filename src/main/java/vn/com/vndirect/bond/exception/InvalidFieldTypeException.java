package vn.com.vndirect.bond.exception;

public class InvalidFieldTypeException extends BusinessException {
    public InvalidFieldTypeException(String message) {
        super(ErrorCode.INVALID_FIELD_TYPE, message);
    }
}
