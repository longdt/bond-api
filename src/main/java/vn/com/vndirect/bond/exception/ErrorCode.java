package vn.com.vndirect.bond.exception;

public interface ErrorCode {
    BusinessErrorCode DEAL_NOT_FOUND =
            new BusinessErrorCode("BOND-1001", "Deal is not found", 404);

    BusinessErrorCode PRODUCT_NOT_FOUND =
            new BusinessErrorCode("BOND-1002", "Product is not found", 404);

    BusinessErrorCode PRODUCT_ID_ALLOW =
            new BusinessErrorCode("BOND-1003", "Product id not allow", 404);

    BusinessErrorCode TABLERATE_NOT_FOUND =
            new BusinessErrorCode("BOND-1004", "TableRate is not found", 404);

    BusinessErrorCode INTERNAL_SERVER_ERROR =
            new BusinessErrorCode("BOND-2001", "Internal Server Error", 500);

    BusinessErrorCode INVALID_DEAL_ID =
            new BusinessErrorCode("BOND-1005", "Invalid deal id", 400);

    BusinessErrorCode REQUIRE_PROD_BONE_CODE_ERROR =
            new BusinessErrorCode("BOND-1006", "Require Product Bone Code Error", 400);

    BusinessErrorCode REQUIRE_PROD_TYPE_ERROR =
            new BusinessErrorCode("BOND-1007", "Require Product Type Error", 400);

    BusinessErrorCode REQUIRE_PROD_LIMIT_ERROR =
            new BusinessErrorCode("BOND-1008", "Require Product Limit Error", 400);

    BusinessErrorCode REQUIRE_PROD_PROMOTION_ERROR =
            new BusinessErrorCode("BOND-1009", "Require Product Promotion Error", 400);

    BusinessErrorCode REQUIRE_PROD_TR_ERROR =
            new BusinessErrorCode("BOND-1010", "Require Product Table Rate Error", 400);

    BusinessErrorCode REQUIRE_TR_RATE_ERROR =
            new BusinessErrorCode("BOND-1011", "Require TR Rate Error", 400);

    BusinessErrorCode REQUIRE_TR_TERM_ERROR =
            new BusinessErrorCode("BOND-1012", "Require TR Term Error", 400);

    BusinessErrorCode REQUIRE_TR_TERM_UNIT_ERROR =
            new BusinessErrorCode("BOND-1013", "Require TR TermUnit Error", 400);

    BusinessErrorCode INVALID_FIELD_TYPE =
            new BusinessErrorCode("BOND-1014", "Invalid field type Error", 400);

    BusinessErrorCode INVALID_BOND_CODE =
            new BusinessErrorCode("BOND-1015", "Invalid bond code error", 400);

    BusinessErrorCode BOND_NOT_FOUND =
            new BusinessErrorCode("BOND-1016", "Bond is not found", 404);

    BusinessErrorCode INVALID_PAGE_REQUEST =
            new BusinessErrorCode("BOND-1017", "Invalid param page/size", 400);

    BusinessErrorCode INVALID_PRODUCT_ID =
            new BusinessErrorCode("BOND-1018", "Invalid product id", 400);

    BusinessErrorCode DUPLICATE_ID_ERROR =
            new BusinessErrorCode("BOND-1019", "Duplicate id Error", 400);

    BusinessErrorCode REQUIRE_TR_TERM_NAME_ERROR =
            new BusinessErrorCode("BOND-1020", "Require TR Name Error", 400);

    BusinessErrorCode REQUIRE_TR_TERMS_ERROR =
            new BusinessErrorCode("BOND-1021", "Require TR TERMS Error", 400);

    BusinessErrorCode INVALID_TABLE_RATE_ID =
            new BusinessErrorCode("BOND-1022", "Invalid table rate id Error", 400);

}
