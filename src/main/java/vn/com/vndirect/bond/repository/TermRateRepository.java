package vn.com.vndirect.bond.repository;

import vn.com.vndirect.bond.model.entity.TermRate;

public interface TermRateRepository extends CrudRepository<Long, TermRate> {
}
