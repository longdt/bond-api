package vn.com.vndirect.bond.repository;

import vn.com.vndirect.bond.model.entity.Deal;

public interface DealRepository extends CrudRepository<Long, Deal> {
}
