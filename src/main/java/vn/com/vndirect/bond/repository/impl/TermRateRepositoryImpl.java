package vn.com.vndirect.bond.repository.impl;

import io.vertx.ext.sql.SQLClient;
import vn.com.vndirect.bond.model.entity.TableRate;
import vn.com.vndirect.bond.model.entity.TermRate;
import vn.com.vndirect.bond.model.entity.TermUnit;
import vn.com.vndirect.bond.repository.TermRateRepository;

import javax.inject.Inject;
import java.time.OffsetDateTime;

public class TermRateRepositoryImpl extends AbstractCrudRepository<Long, TermRate> implements TermRateRepository {

    @Inject
    public TermRateRepositoryImpl(SQLClient sqlClient) {
        Config<Long, TermRate> conf = new Config.Builder<Long, TermRate>("TermRate", TermRate::new)
                .pk("termRateId", TermRate::getTermRateId, TermRate::setTermRateId, true)
                .addField("term", TermRate::getTerm, TermRate::setTerm)
                .addField("rate", TermRate::getRate, TermRate::setRate)
                .addField("termUnit", tr -> tr.getTermUnit() == null ? null : tr.getTermUnit().getValue(),
                        (tr, o) -> {
                            if (o != null) tr.setTermUnit(TermUnit.of(o.toString()));
                        })
                .addField("tableRateId", TermRate::getTableRateId, TermRate::setTableRateId)
                .addTimestampTzField("createdDate", TermRate::getCreatedDate, TermRate::setCreatedDate)
                .addTimestampTzField("updatedDate", TermRate::getUpdatedDate, TermRate::setUpdatedDate)
                .build();
        init(sqlClient, conf);
    }
}
