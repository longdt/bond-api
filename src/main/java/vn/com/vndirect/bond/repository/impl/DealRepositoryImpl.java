package vn.com.vndirect.bond.repository.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import io.vertx.ext.sql.SQLClient;
import vn.com.vndirect.bond.model.entity.Deal;
import vn.com.vndirect.bond.model.entity.TermRate;
import vn.com.vndirect.bond.repository.DealRepository;

import javax.inject.Inject;
import java.util.List;

public class DealRepositoryImpl extends AbstractCrudRepository<Long, Deal> implements DealRepository {

    @Inject
    public DealRepositoryImpl(SQLClient sqlClient) {
        Config<Long, Deal> conf = new Config.Builder<Long, Deal>("Deal", Deal::new)
                .pk("dealId", Deal::getDealId, Deal::setDealId, true)
                .addField("accountNo", Deal::getAccountNo, Deal::setAccountNo)
                .addField("brokerAccountNo", Deal::getBrokerAccountNo, Deal::setBrokerAccountNo)
                .addField("side", Deal::getSide, Deal::setSide)
                .addField("bondCode", Deal::getBondCode, Deal::setBondCode)
                .addField("tenor", Deal::getTenor, Deal::setTenor)
                .addField("productRate", Deal::getProductRate, Deal::setProductRate)
                .addField("quantity", Deal::getQuantity, Deal::setQuantity)
                .addDecimalField("price", Deal::getPrice, Deal::setPrice)
                .addField("yield", Deal::getYield, Deal::setYield)
                .addField("isLeg2", Deal::getLeg2, Deal::setLeg2)
                .addJsonField("terms", Deal::getTerms, Deal::setTerms, new TypeReference<List<TermRate>>() {
                })
                .build();

        init(sqlClient, conf);
    }
}
