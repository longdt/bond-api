package vn.com.vndirect.bond.repository;

import vn.com.vndirect.bond.model.entity.Product;

public interface ProductRepository extends CrudRepository<String, Product> {

}

