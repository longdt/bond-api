package vn.com.vndirect.bond.repository;

import vn.com.vndirect.bond.model.entity.TableRate;

public interface TableRateRepository extends CrudRepository<Long, TableRate> {

}
