package vn.com.vndirect.bond.repository.impl;

import io.vertx.ext.sql.SQLClient;
import vn.com.vndirect.bond.model.entity.Product;
import vn.com.vndirect.bond.model.entity.ProductType;
import vn.com.vndirect.bond.repository.ProductRepository;

import javax.inject.Inject;

public class ProductRepositoryImpl extends AbstractCrudRepository<String, Product> implements ProductRepository {
    @Inject
    public ProductRepositoryImpl(SQLClient sqlClient) {
        Config<String, Product> conf = new Config.Builder<String, Product>("Product", Product::new)
                .pk("productId", Product::getProductId, Product::setProductId)
                .addField("bondCode", Product::getBondCode, Product::setBondCode)
                .addField("productType", product -> product.getProductType() == null? null : product.getProductType().getValue(),
                        (product, o) -> {
                            if(o != null ) {
                                product.setProductType(ProductType.of(o));
                            }
                        })
                .addField("limit", Product::getLimit, Product::setLimit)
                .addField("creator", Product::getCreator, Product::setCreator)
                .addTimestampTzField("createdDate", Product::getCreatedDate, Product::setCreatedDate)
                .addTimestampTzField("updatedDate", Product::getUpdatedDate, Product::setUpdatedDate)
                .addField("tableRateId", Product::getTableRateId, Product::setTableRateId)
                .build();
        init(sqlClient, conf);
    }
}
