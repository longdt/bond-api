package vn.com.vndirect.bond.repository.impl;

import io.vertx.ext.sql.SQLClient;
import vn.com.vndirect.bond.model.entity.TableRate;
import vn.com.vndirect.bond.repository.TableRateRepository;

import javax.inject.Inject;

public class TableRateRepositoryImpl extends AbstractCrudRepository<Long, TableRate> implements TableRateRepository {
    @Inject
    public TableRateRepositoryImpl(SQLClient sqlClient) {
        Config<Long, TableRate> conf = new Config.Builder<Long, TableRate>("TableRate", TableRate::new)
                .pk("tableRateId", TableRate::getTableRateId, TableRate::setTableRateId, true)
                .addField("name", TableRate::getName, TableRate::setName)
                .addTimestampTzField("createdDate", TableRate::getCreatedDate, TableRate::setCreatedDate)
                .addTimestampTzField("updatedDate", TableRate::getUpdatedDate, TableRate::setCreatedDate)
                .build();
        init(sqlClient, conf);
    }
}
