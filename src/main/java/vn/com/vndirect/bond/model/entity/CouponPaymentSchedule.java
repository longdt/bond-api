package vn.com.vndirect.bond.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class CouponPaymentSchedule {
    /*
    * Lịch trả lãi
    * */

    /*
    * Ngày hiệu lực
    * */
    @JsonProperty("value_date")
    private Date valueDate;
    @JsonProperty("reference_interest")
    private double referenceInterest;
    @JsonProperty("limit_interest")
    private double limitInterest;
    @JsonProperty("bond_code")
    private String bondCode;

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public double getReferenceInterest() {
        return referenceInterest;
    }

    public void setReferenceInterest(double referenceInterest) {
        this.referenceInterest = referenceInterest;
    }

    public double getLimitInterest() {
        return limitInterest;
    }

    public void setLimitInterest(double limitInterest) {
        this.limitInterest = limitInterest;
    }

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode;
    }
}
