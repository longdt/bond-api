package vn.com.vndirect.bond.model.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ProductType {
    OUTRIGHT("OUTRIGHT"), VAR("VAR"), CLASSIC_REPO("CLASSIC_REPO"), FIX("FIX");

    private String value;

    ProductType(String value){ this.value = value;}

    @JsonCreator
    public static ProductType of(String value) {
        switch (value) {
            case "OUTRIGHT":
                return OUTRIGHT;
            case "VAR":
                return VAR;
            case "CLASSIC_REPO":
                return CLASSIC_REPO;
            case "FIX":
                return FIX;
            default: return null;
        }
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
