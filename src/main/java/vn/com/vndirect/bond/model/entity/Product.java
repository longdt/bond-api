package vn.com.vndirect.bond.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;
import java.util.List;

public class Product {
    @JsonProperty("product_id")
    private String productId;
    @JsonProperty("bond_code")
    private String bondCode;
    @JsonProperty("product_type")
    private ProductType productType;
    private Integer limit;
    @JsonProperty("max_promotion")
    private double promotion;
    private String creator;
    @JsonProperty("created_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private OffsetDateTime createdDate;
    @JsonProperty("updated_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private OffsetDateTime updatedDate;
    @JsonProperty("table_rate_id")
    private Long tableRateId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public double getPromotion() {
        return promotion;
    }

    public void setPromotion(double promotion) {
        this.promotion = promotion;
    }

    public String getCreator() { return creator; }

    public void setCreator(String creator) { this.creator = creator; }

    public OffsetDateTime getCreatedDate() { return createdDate; }

    public void setCreatedDate(OffsetDateTime createdDate) { this.createdDate = createdDate; }

    public OffsetDateTime getUpdatedDate() { return updatedDate; }

    public void setUpdatedDate(OffsetDateTime updatedDate) { this.updatedDate = updatedDate; }

    public Long getTableRateId() {
        return tableRateId;
    }

    public void setTableRateId(Long tableRateId) {
        this.tableRateId = tableRateId;
    }
}