package vn.com.vndirect.bond.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class Deal {
    @JsonProperty("deal_id")
    private Long dealId;
    @JsonProperty("account_no")
    private String accountNo;
    @JsonProperty("broker_account_no")
    private String brokerAccountNo;
    @JsonProperty("side")
    private String side;
    @JsonProperty("bond_code")
    private String bondCode;
    @JsonProperty("product_type")
    private String productType;
    @JsonProperty("tenor")
    private Float tenor;
    @JsonProperty("product_rate")
    private Float productRate;
    @JsonProperty("quantity")
    private Long quantity;
    @JsonProperty("price")
    private BigDecimal price;
    @JsonProperty("yield")
    private Float yield;
    @JsonProperty("is_leg2")
    private Boolean isLeg2;
    private BigDecimal promotion;
    private List<TermRate> terms;

    public Long getDealId() {
        return dealId;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBrokerAccountNo() {
        return brokerAccountNo;
    }

    public void setBrokerAccountNo(String brokerAccountNo) {
        this.brokerAccountNo = brokerAccountNo;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Float getTenor() {
        return tenor;
    }

    public void setTenor(Float tenor) {
        this.tenor = tenor;
    }

    public Float getProductRate() {
        return productRate;
    }

    public void setProductRate(Float productRate) {
        this.productRate = productRate;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Float getYield() {
        return yield;
    }

    public void setYield(Float yield) {
        this.yield = yield;
    }

    public Boolean getLeg2() {
        return isLeg2;
    }

    public void setLeg2(Boolean leg2) {
        isLeg2 = leg2;
    }

    public BigDecimal getPromotion() {
        return promotion;
    }

    public void setPromotion(BigDecimal promotion) {
        this.promotion = promotion;
    }

    public List<TermRate> getTerms() {
        return terms;
    }

    public void setTerms(List<TermRate> terms) {
        this.terms = terms;
    }
}
