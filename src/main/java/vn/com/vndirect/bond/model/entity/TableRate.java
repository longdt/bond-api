package vn.com.vndirect.bond.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;

public class TableRate {
    @JsonProperty("table_rate_id")
    private Long tableRateId;
    private String name;
    @JsonProperty("created_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private OffsetDateTime createdDate;
    @JsonProperty("updated_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private OffsetDateTime updatedDate;

    public Long getTableRateId() {
        return tableRateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTableRateId(Long tableRateId) {
        this.tableRateId = tableRateId;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public OffsetDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(OffsetDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }
}
