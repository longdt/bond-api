package vn.com.vndirect.bond.model.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.stream.Stream;

public enum TermUnit {
    DAY('D'), MONTH('M'), YEAR('Y');

    private char value;

    TermUnit(char value) {
        this.value = value;
    }

    @JsonCreator
    public static TermUnit of(String value) {
        switch (value) {
            case "D":
                return DAY;
            case "M":
                return MONTH;
            case "Y":
                return YEAR;
                default: return null;
        }
    }

    @JsonValue
    public char getValue() {
        return value;
    }
}