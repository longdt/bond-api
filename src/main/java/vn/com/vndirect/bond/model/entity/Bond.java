package vn.com.vndirect.bond.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class Bond {
    private String bondCode;
    private LocalDate maturityDate;
    private double bondCouponRate;
    private LocalDate issueDate;
    private double parValue;

    @JsonProperty("remain_value")
    private double remainValue;

    @JsonProperty("day_count_basic")
    private int dayCountBasic;

    @JsonProperty("bond_code")
    public String getBondCode() {
        return bondCode;
    }

    @JsonProperty("symbol")
    public void setBondCode(String bondCode) {
        this.bondCode = bondCode;
    }

    @JsonProperty("maturity_date")
    @JsonFormat(shape=JsonFormat.Shape.STRING)
    public LocalDate getMaturityDate() {
        return maturityDate;
    }

    @JsonProperty("maturityDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    public void setMaturityDate(LocalDate maturityDate) {
        this.maturityDate = maturityDate;
    }

    @JsonProperty("bond_coupon_rate")
    public double getBondCouponRate() {
        return bondCouponRate;
    }

    @JsonProperty("bondCouponRate")
    public void setBondCouponRate(double bondCouponRate) {
        this.bondCouponRate = bondCouponRate;
    }

    @JsonProperty("issue_date")
    @JsonFormat(shape=JsonFormat.Shape.STRING)
    public LocalDate getIssueDate() {
        return issueDate;
    }

    @JsonProperty("issueDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    public void setIssueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
    }

    @JsonProperty("par_value")
    public double getParValue() {
        return parValue;
    }

    @JsonProperty("parValue")
    public void setParValue(double parValue) {
        this.parValue = parValue;
    }

    public double getRemainValue() {
        return remainValue;
    }

    public void setRemainValue(double remainValue) {
        this.remainValue = remainValue;
    }

    public int getDayCountBasic() {
        return dayCountBasic;
    }

    public void setDayCountBasic(int dayCountBasic) {
        this.dayCountBasic = dayCountBasic;
    }
}
