package vn.com.vndirect.bond.model.request;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import vn.com.vndirect.bond.model.entity.TableRate;
import vn.com.vndirect.bond.model.entity.TermRate;

import java.util.List;

public class TableRateRequest {
    @JsonUnwrapped
    private TableRate tableRate;
    private List<TermRate> terms;

    public TableRate getTableRate() {
        return tableRate;
    }

    public void setTableRate(TableRate tableRate) {
        this.tableRate = tableRate;
    }

    public List<TermRate> getTerms() {
        return terms;
    }

    public void setTerms(List<TermRate> terms) {
        this.terms = terms;
    }
}
