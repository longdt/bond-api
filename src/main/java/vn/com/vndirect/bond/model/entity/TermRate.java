package vn.com.vndirect.bond.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;

public class TermRate {
    @JsonProperty("term_rate_id")
    private Long termRateId;
    @JsonProperty("table_rate_id")
    private Long tableRateId;
    @JsonProperty("term_unit")
    private TermUnit termUnit;
    private Integer term;
    private Float rate;
    @JsonProperty("created_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private OffsetDateTime createdDate;
    @JsonProperty("updated_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private OffsetDateTime updatedDate;

    public Long getTermRateId() {
        return termRateId;
    }

    public void setTermRateId(Long termRateId) {
        this.termRateId = termRateId;
    }

    public Long getTableRateId() {
        return tableRateId;
    }

    public void setTableRateId(Long tableRateId) {
        this.tableRateId = tableRateId;
    }

    public TermUnit getTermUnit() {
        return termUnit;
    }

    public void setTermUnit(TermUnit termUnit) {
        this.termUnit = termUnit;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public OffsetDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(OffsetDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }
}
