package vn.com.vndirect.bond.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class PaymentSchedule {

    /*
    * Lịch trả gốc
    * */
    @JsonProperty("value_date")
    private Date valueDate;
    private double rate;
    @JsonProperty("bond_code")
    private String bondCode;

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode;
    }
}
