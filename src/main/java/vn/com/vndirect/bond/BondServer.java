package vn.com.vndirect.bond;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.LoggerHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vn.com.vndirect.bond.conf.AppComponent;
import vn.com.vndirect.bond.conf.AppModule;
import vn.com.vndirect.bond.conf.DaggerAppComponent;
import vn.com.vndirect.bond.http.handler.DealHandler;
import vn.com.vndirect.bond.http.handler.ExceptionHandler;
import vn.com.vndirect.bond.http.handler.ProductHandler;
import vn.com.vndirect.bond.http.handler.TableRateHandler;

public class BondServer {
    private static final Logger logger = LogManager.getLogger(BondServer.class);
    private Vertx vertx;
    private HttpServer server;

    public BondServer() {
        vertx = Vertx.vertx();
        server = vertx.createHttpServer();
        init();
    }

    private void init() {
        Router router = Router.router(vertx);
        AppComponent creator = DaggerAppComponent.builder().appModule(new AppModule(vertx)).build();
        router.route().handler(LoggerHandler.create());
        router.route().handler(BodyHandler.create());
        router.route().failureHandler(ExceptionHandler::handle);
        ProductHandler productHandler = creator.createProductHandler();
        DealHandler dealHandler = creator.createDealHandler();
        TableRateHandler tableRateHandler = creator.createTableRateHandler();
        router.post("/v1/products").handler(productHandler::createProduct);
        router.get("/v1/products").handler(productHandler::getProductPage);
        router.put("/v1/products/:id").handler(productHandler::updateProduct);
        router.get("/v1/products/:id").handler(productHandler::getProduct);
        router.get("/v1/products/:productType/:bondCode").handler(productHandler::getProductBy);
        router.delete("/v1/products/:id").handler(productHandler::deleteProduct);
        router.post("/v1/deals").handler(dealHandler::createDeal);
        router.get("/v1/deals").handler(dealHandler::getDealPage);
        router.get("/v1/deals/:id").handler(dealHandler::getDeal);
        router.post("/v1/table_rates").handler(tableRateHandler::createTableRate);
        router.get("/v1/table_rates/:id").handler(tableRateHandler::getTableRate);
        server.requestHandler(router::accept);
    }

    public void start(int port) {
        server.listen(port, event -> {
            if (event.succeeded()) {
                logger.info("Server started on {} port", event.result().actualPort());
            } else {
                logger.error("Cant start server on {} port", port, event.cause());
            }
        });
    }

    public void stop() {
        server.close(event -> {
            if (event.succeeded()) {
                logger.info("Server was stopped");
            } else {
                logger.error("Cant stop server", event.cause());
            }
        });
    }

    public static void main(String[] args) {
        Json.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Json.mapper.registerModule(new JavaTimeModule());
        Json.mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        new BondServer().start(8080);
    }
}
