package vn.com.vndirect.bond.pricing;

import java.math.BigDecimal;

public class PricingService {

    public BigDecimal coupon(BigDecimal remainParValue, BigDecimal couponInterest, int frequencyOfInterestPayment) {
        return remainParValue.multiply(couponInterest).multiply(new BigDecimal(frequencyOfInterestPayment))
                .setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal principalPayment(BigDecimal parValue, BigDecimal ratePayment) {
        return parValue.multiply(ratePayment);
    }

    public BigDecimal presentValueOfCash(BigDecimal coupon, BigDecimal principalPayment, double yield, int x) {
        return coupon.add(principalPayment).divide(BigDecimal.ONE.add(new BigDecimal(yield/x)))
                .setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}
